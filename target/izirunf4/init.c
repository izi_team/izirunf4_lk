/*
 * Copyright (c) 2020 Mihail Cherciu
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <dev/gpio.h>
#include <dev/i2c.h>
#include <platform/gpio.h>
#include <platform/debug_uart.h>
#include <platform/adc.h>
#include <platform/dma.h>
#include <platform/spi.h>
#include <target/gpioconfig.h>

#define LOG_MODULE_NAME "INIT"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO /* | LOG_DEBUG */ )
#include "log/log.h"

void target_early_init(void)
{
    /* configure status leds */
    gpio_config(GPIO_LED_GREEN, GPIO_MODE_OUT_PP);
    gpio_set(GPIO_LED_GREEN,    1);

    /* configure button */
    gpio_config(GPIO_BTN_USER,  GPIO_MODE_IN);

    /* configure gpio SPI */
    gpio_config(GPIO_SPI2_CS0,  GPIO_MODE_OUT_PP);
    gpio_set(GPIO_SPI2_CS0,     1);
    gpio_config(GPIO_SPI2_CS1,  GPIO_MODE_OUT_PP);
    gpio_set(GPIO_SPI2_CS1,     1);
    gpio_config(GPIO_SPI2_CS2,  GPIO_MODE_OUT_PP);
    gpio_set(GPIO_SPI2_CS2,     1);
    gpio_config(GPIO_SPI3_CS,   GPIO_MODE_OUT_PP);
    gpio_set(GPIO_SPI3_CS,      1);

    /* configure gpio adc */
    gpio_config(GPIO_ADC0,      GPIO_MODE_ANA);
    gpio_config(GPIO_ADC1,      GPIO_MODE_ANA);

    /* configure gpio IO */
    gpio_config(GPIO_IO0,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO1,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO2,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO3,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO4,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO5,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO6,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO7,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO8,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO9,      GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO10,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO11,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO12,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO13,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO14,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO15,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO16,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO17,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO18,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO19,     GPIO_MODE_OUT_PP);
    gpio_config(GPIO_IO20,     GPIO_MODE_OUT_PP);

    /* TODO: pwm implementation */

    /* configure share interface */
    /* I2C1 */
    gpio_config(GPIO_I2C1_SDA,  GPIO_AFn(4) | GPIO_MODE_ALT_F_OD | GPIO_PULL_UP);
    gpio_config(GPIO_I2C1_CLK,  GPIO_AFn(4) | GPIO_MODE_ALT_F_OD | GPIO_PULL_UP);
    /* I2C2 */
    gpio_config(GPIO_I2C3_SDA,  GPIO_AFn(4) | GPIO_MODE_ALT_F_OD | GPIO_PULL_UP);
    gpio_config(GPIO_I2C3_CLK,  GPIO_AFn(4) | GPIO_MODE_ALT_F_OD | GPIO_PULL_UP);

    /* SPI2 */
    gpio_config(GPIO_SPI2_SCK,  GPIO_AFn(5) | GPIO_MODE_ALT_F_PP);
    gpio_config(GPIO_SPI2_MISO, GPIO_AFn(5) | GPIO_MODE_ALT_F_PP);
    gpio_config(GPIO_SPI2_MOSI, GPIO_AFn(5) | GPIO_MODE_ALT_F_PP);
    /* SPI3 */
    gpio_config(GPIO_SPI3_SCK,  GPIO_AFn(6) | GPIO_MODE_ALT_F_PP);
    gpio_config(GPIO_SPI3_MISO, GPIO_AFn(6) | GPIO_MODE_ALT_F_PP);
    gpio_config(GPIO_SPI3_MOSI, GPIO_AFn(6) | GPIO_MODE_ALT_F_PP);

    /* configure the usart1 pins - console uart*/
    gpio_config(GPIO_UART1_TX, GPIO_AFn(7) | GPIO_MODE_ALT_F_PP | GPIO_PULL_UP);
    gpio_config(GPIO_UART1_RX, GPIO_AFn(7) | GPIO_MODE_ALT_F_PP | GPIO_PULL_UP);

    /* configure the usart2 pins */
    gpio_config(GPIO_UART2_TX, GPIO_AFn(7) | GPIO_MODE_ALT_F_PP | GPIO_PULL_UP);
    gpio_config(GPIO_UART2_RX, GPIO_AFn(7) | GPIO_MODE_ALT_F_PP | GPIO_PULL_UP);

    /* configure the usart3 pins */
    gpio_config(GPIO_UART3_TX, GPIO_AFn(7) | GPIO_MODE_ALT_F_PP | GPIO_PULL_UP);
    gpio_config(GPIO_UART3_RX, GPIO_AFn(7) | GPIO_MODE_ALT_F_PP | GPIO_PULL_UP);

    debug_early_init();
    dma_init_early();
    adc_init_early();
    i2c_init_early();
    spi_init_early();
}


void target_init(void)
{
    debug_init();
    dma_init();
    adc_init();
    i2c_init();
    spi_init();
}

