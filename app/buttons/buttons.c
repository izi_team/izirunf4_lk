/*
 * Copyright (c) 2020 Mihail Cherciu
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <system.h>
#include <app.h>
#include <platform.h>
#include <dev/gpio.h>
#include <target/gpioconfig.h>

#define LOG_MODULE_NAME "BTN"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO | LOG_DEBUG)
#include "log/log.h"

typedef enum {
    UNPRESSED = 0,
    PRESSED
} button_state_t;

typedef struct {
    button_state_t state;
    lk_time_t press_time;
    lk_time_t release_time;
    lk_time_t elapsed_time;
} button_t;

thread_t *threadMainButton;

button_t user_btn;

static int main_button(void *arg) {
    while(true) {

        /* User button */
        if (gpio_get(GPIO_BTN_USER) == false) {
            if (user_btn.state == UNPRESSED) {
                user_btn.state = PRESSED;
                user_btn.press_time = current_time();
            }
        }
        else {
            if (user_btn.state == PRESSED) {
                user_btn.release_time = current_time();
                user_btn.elapsed_time = user_btn.release_time - user_btn.press_time;

                print_debug("BTN USER clicked in: %d ms\n", user_btn.elapsed_time);

                if (user_btn.elapsed_time > 1000) {
                    print_debug("Long push\n");
                }
                else {
                    print_debug("Short push\n");
                }
            }
            user_btn.state = UNPRESSED;
        }

        thread_sleep(100);
    }

    return 0;
}

static void buttons_init(const struct app_descriptor *app)
{
    print_debug("Init Button...\n");

    /* Init button state */
    user_btn.state = UNPRESSED;

    /* Create thread */
    threadMainButton = thread_create("button thread", &main_button, NULL, DEFAULT_PRIORITY + 1, 1024);
    thread_resume(threadMainButton);
}

APP_START(buttons)
.init   = buttons_init,
APP_END
