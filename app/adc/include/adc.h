#ifndef ADC_H_
#define ADC_H_

typedef enum {
    IZIRUNF4_ADC0 = 3,       //IZIRUNF0_ADC0 is mapping to ADC3
    IZIRUNF4_ADC1 = 6,       //IZIRUNF0_ADC0 is mapping to ADC6
    IZIRUNF4_VREF = 17       //Internal adc vref
} adc_ch_t;

#define ADC_MIN             0U
#define ADC_MAX             4095U

#define ADC_MIN_VOLTAGE     0
#define ADC_MAX_VOLTAGE     3.4

#endif  // ADC_H_
