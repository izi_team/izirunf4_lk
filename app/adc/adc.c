/*
 * Copyright (c) 2019-2020 IZITRON (Mihail Cherciu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <system.h>
#include <app.h>
#include <platform/adc.h>
#include "adc.h"

uint16_t adc_buffer[ADC_CBUF_SIZE];
static void adc(uint32_t channel, uint32_t interval, uint32_t steps);
static void adc_buf(uint32_t channel, uint32_t length);
static void adc_vol(void);

#define LOG_MODULE_NAME "ADC"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO | LOG_DEBUG)
#include "log/log.h"

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>
static int adc_cmd(int argc, const console_cmd_args *argv);
static int adc_buf_cmd(int argc, const console_cmd_args *argv);
static int adc_vol_cmd(int argc, const console_cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("adc", "adc read one value", (console_cmd_func)&adc_cmd)
STATIC_COMMAND("adc_buf", "adc a buffer length", (console_cmd_func)&adc_buf_cmd)
STATIC_COMMAND("adc_vol", "get voltage system", (console_cmd_func)&adc_vol_cmd)
STATIC_COMMAND_END(adc);

static int adc_cmd(int argc, const console_cmd_args *argv)
{
    if (argc < 4) {
        printf("not enough arguments !\n");
        usage:
            printf("%s <izirunf0_adc_channel> <interval(ms)> <repeat_nr>\n", argv[0].str);
            goto out;
    }

    if (((argv[1].u == 0) || (argv[1].u == 1)) && (argv[2].u != 0) && (argv[3].u != 0)) {
        adc((uint32_t) argv[1].u, (uint32_t) argv[2].u, (uint32_t) argv[3].u);
    }
    else {
        goto usage;
    }

    out:
        return 0;
}

static int adc_buf_cmd(int argc, const console_cmd_args *argv)
{
    if (argc < 3) {
        printf("not enough arguments !\n");
        usage:
            printf("%s <izirunf0_adc_channel> <length buffer>\n", argv[0].str);
            goto out;
    }

    if (((argv[1].u == 0) || (argv[1].u == 1) || (argv[1].u == 17)) && (argv[2].u != 0)) {
        adc_buf((uint32_t) argv[1].u, (uint32_t) argv[2].u);
    }
    else {
        goto usage;
    }

    out:
        return 0;
}

static int adc_vol_cmd(int argc, const console_cmd_args *argv)
{
    if (argc != 1) {
        printf("arguments error!\n");
        usage:
            printf("%s \n", argv[0].str);
            goto out;
    }

    adc_vol();

    out:
        return 0;
}

#endif

static uint8_t adc_to_percent(uint16_t adc_val) {
    float percent = 0;

    if (adc_val < ADC_MIN) return 0;
    if (adc_val > ADC_MAX) return 100;
    percent = ((adc_val - ADC_MIN)*100) / (float)(ADC_MAX - ADC_MIN);

    return (uint8_t)percent;
}

static float adc_to_voltage(uint16_t adc_val) {
    float voltage = 0;

    voltage = ((adc_val * ADC_MAX_VOLTAGE) / (float)(ADC_MAX));

    return voltage;
}

static void adc(uint32_t channel, uint32_t interval, uint32_t steps) {
    adc_ch_t adc_ch;
    uint32_t i;
    uint16_t adc;
    uint8_t  percent;
    float voltage;

    /* Map to iziboard adc pins */
    if (channel == 0) adc_ch = IZIRUNF4_ADC0;
    else if (channel == 1) adc_ch = IZIRUNF4_ADC1;
    else if (channel == 17) adc_ch = IZIRUNF4_VREF;
    else return;

    printf("-------------------------------------------------\n");
    printf("|Channel| Value\t| Voltage\t| Percent| Step\t|\n");
    printf("-------------------------------------------------\n");
    for (i = 1; i <= steps; i++) {
        adc = get_adc_value(adc_ch);
        percent = adc_to_percent(adc);
        voltage = adc_to_voltage(adc);
        printf("| ADC%d \t| %d \t| %f \t| %d%% \t| %d \t|\n",
                channel, adc, voltage, percent, i);
        thread_sleep(interval);
    }
    printf("-------------------------------------------------\n");
}

static void adc_buf(uint32_t channel, uint32_t length) {
    adc_ch_t adc_ch;
    uint32_t i;
    uint8_t  percent;
    float voltage;

    printf("Channel %d\n", channel);
    /* Map to iziboard adc pins */
    if (channel == 0) adc_ch = IZIRUNF4_ADC0;
    else if (channel == 1) adc_ch = IZIRUNF4_ADC1;
    else if (channel == 17) adc_ch = IZIRUNF4_VREF;
    else return;

    get_adc_buffer(adc_ch, adc_buffer, length);

    printf("-------------------------------------------------\n");
    printf("|Channel| Value\t| Voltage\t| Percent| Step\t|\n");
    printf("-------------------------------------------------\n");
    for (i = 0; i < length; i++) {
        percent = adc_to_percent(adc_buffer[i]);
        voltage = adc_to_voltage(adc_buffer[i]);
        printf("| ADC%d \t| %d \t| %f \t| %d%% \t| %d \t|\n",
                channel, adc_buffer[i], voltage, percent, i);
    }
    printf("-------------------------------------------------\n");
}

static void adc_vol(void) {
    uint32_t i;
    uint16_t len;
    uint8_t  percent;
    uint32_t mean_adc;
    float voltage;

    if (ADC_CBUF_SIZE > 10) len = 10;
    else len = ADC_CBUF_SIZE;

    get_adc_buffer(ADC_VREFINT, adc_buffer, len);

    mean_adc = 0;
    for (i = 0; i < len; i++) {
        mean_adc += adc_buffer[i];
    }
    mean_adc = (uint32_t)((float)mean_adc/len);
    printf("MEAN ADC for %d values: %d\n",len, mean_adc);
    printf("VREFINT_CAL_ADDR: %d\n", (*VREFINT_CAL_ADDR));

    voltage = VREFINT_CAL_VREF * (*VREFINT_CAL_ADDR)/(float)mean_adc;

    printf("ADC SYSTEM VOLTAGE: %f V\n", voltage);
}
