#ifndef SENSORS_H_
#define SENSORS_H_

/* use am2320 sensor */
#ifdef USE_DEVICE_AM2320
#define AM2320_BUS              1
#define AM2320_I2C_ADDRESS      0xB8
#endif

#endif /* SENSORS_H_ */
