/*
 * Copyright (c) 2020 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <system.h>
#include <app.h>
#include <string.h>
#include <dev/gpio.h>
#include <target/gpioconfig.h>
#include <platform/spi.h>
#include "w25xx0cl.h"
#include "flash.h"

#define LOG_MODULE_NAME "FLASH"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO)//  | LOG_DEBUG)
#include "log/log.h"

flash_w25xx0cl_device_t flash;

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int flash_console(int argc, const console_cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("flash", "manage flash", (console_cmd_func)&flash_console)
STATIC_COMMAND_END(flash);

static int flash_console(int argc, const console_cmd_args *argv)
{
    if (argc < 2) {
usage:
        printf("not enough arguments !\n");
        printf("%s <read/write> <addr mem> <value>\n", argv[0].str);
        printf("%s <id>\n", argv[0].str);
        goto out;
    }

    if (!strcmp(argv[1].str, "write")) {
        if (argc < 4) goto usage;
        print_debug("FLASH write\n");
        uint32_t addr = argv[2].u;
        uint8_t value = argv[3].u;
        flash.write(&flash, addr, &value, 1);

    }
    else if (!strcmp(argv[1].str, "read")) {
        if (argc < 3) goto usage;
        print_debug("FLASH read\n");
        uint32_t addr = argv[2].u;
        uint8_t value;
        flash.read(&flash, addr, &value, 1);
        printf("%d\n", value);
    }
    else if (!strcmp(argv[1].str, "id")) {
        if (argc < 2) goto usage;
        print_debug("FLASH get id\n");
        flash.get_id(&flash);
    }
    else
    {
        goto usage;
    }

out:
    return 0;
}

#endif

static void flash_init(const struct app_descriptor *app)
{
    print_info("Init Flash...\n");
    /* Init flash event */

    flash_w25xx0cl_init(&flash, FLASH_BUS, GPIO_SPI3_CS);
}

APP_START(flash)
.init   = flash_init,
APP_END
