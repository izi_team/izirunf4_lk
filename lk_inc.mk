LOCAL_DIR := izirunf4_lk
LKMAKEROOT := ..
LKROOT := lk
DEVICES_LK ?= devices_lk
LKINC := $(LOCAL_DIR) $(DEVICES_LK)
DEFAULT_PROJECT ?= izirunf4_izigoboard
BUILDROOT ?= $(LOCAL_DIR)
TOOLCHAIN_PREFIX = arm-none-eabi-
