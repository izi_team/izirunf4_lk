MODULES += \
	app/system \
	app/shell \
	app/leds \
	app/buttons \
	app/eeprom \
	app/flash \
	app/adc \
	app/sensors \
	lib/version \
	lib/log \
	lib/cmd

include project/target/izirunf4.mk

MODULE_DEPS += 	dev/memory/24cwxx \
				dev/memory/w25xx0cl \
				dev/temp_hum/am2320
EXTRA_BUILDRULES += $(DEVICES_LK)/dev/memory/24cwxx/rules.mk \
					$(DEVICES_LK)/dev/memory/w25xx0cl/rules.mk \
					$(DEVICES_LK)/dev/temp_hum/am2320/rules.mk
